import numpy as np

#---------------------------------
chunks=['06']
sub_systems=['ASC','CAL','HPI','IMC','ISI','LSC','OMC','PEM','PSL','SQZ','SUS','TCS']
for i in chunks: 
    length=np.loadtxt("/home/kalista.wayt/public_html/Unseen_data_test/ANN_ASC/chunk{}/test_predictions/predictions.txt".format(i))
    length=len(length)
    predictions=np.zeros((length))
    print("Shape of predictions:", predictions.shape)
    for j in sub_systems:
        load_path="/home/kalista.wayt/public_html/Unseen_data_test/ANN_{}/chunk{}/test_predictions/predictions.txt".format(j,i)
        sub_system_pred=np.loadtxt(load_path)
        print("Shape of sub_system_pred:", sub_system_pred.shape)
        predictions=np.column_stack((predictions,sub_system_pred))
        print("Shape of predictions:", predictions.shape)

    print(predictions)
    predictions=np.delete(predictions, 0,1)
    print(predictions)
    print("Shape of Predcitions should be (x,12):",predictions.shape)
    np.save("/home/kalista.wayt/predictions_data_combined/unseen_test_data/chunk{}/balanced_test_on_hypertuned_models".format(i),predictions)


