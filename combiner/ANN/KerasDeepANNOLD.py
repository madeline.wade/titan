
__usage__ = "python3 KerasDeepANN.py config.ini"
__description__ = "an ANN for predicting the presence of glitches in strain data using auxiliary channels"
__author__ = "Kenyon LIGO lab"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#--------------------------------------------------------------------------------------------------------
import numpy as np
import pandas as pd

from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, LambdaCallback
from keras.regularizers import l1, l2,l1_l2
from keras.backend import clear_session
from keras.models import load_model

from sklearn import metrics
from sklearn.utils import class_weight
from sklearn.preprocessing import normalize
from sklearn.utils import shuffle

import os
import sys
import configparser
from optparse import OptionParser

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib.collections import PatchCollection
#--------------------------------------------------------------------------------------------------------

#Config and option parser
parser = OptionParser(usage=__usage__, description=__description__)

opts, args = parser.parse_args()

config = configparser.ConfigParser()                           #set up the config parser by setting a variable equal to the ConfigParser class
config.read(args[0])                                           #read in config file from command line



#--------------------------------------------------------------------------------------------------------

def read_data(DATA, balanced=True):
    '''
    read_data:
        This function is a complete function that reads and seperates data into inputs
        and outputs for mla
        
        Inputs:
            -DATA: the directory path to the data
            -balanced: balance the data
    
        Outputs:
            -inputs: features for MLA to train on
            -labels: labels for each feature (0 or 1, where 0 means clean time and 1 means glitch)
    
    '''
    # Reading in the .pat file
    data = pd.read_csv(DATA,delim_whitespace=True, header=1, index_col=False)     #Header=1 says to read the column names from the values in the 2nd row (index = 1), the delim_whitespace=True recognizes spaces as column separations and index_col=False forces Pandas to recognize the labels in the one column as labels for the 16 columns. 
    # Identify important columns
    labels = np.array(data.loc[:,"unclean"])  #isolates the clean/unclean column and turns it into a row
    label_indices = labels.argsort()[::-1]    #create a list of indices that will sort the labels in reverse numerical order 
    labels = labels[label_indices]            #sort the labels in reverse numerical order using the generated list
    labels = labels.reshape(len(labels), 1)   #turn the labels row into a column
    print("Labels:", labels)
    print("Sum of labels:", labels.sum())
    #gps_s = np.array(data.loc[:,"GPS_s"])          #Setting gps_s, gps_ms, etc. to be their respective columns and turning them into columns
    #gps_s = gps_s.reshape(len(gps_s), 1)
    #gps_ms = np.array(data.loc[:,"GPS_ms"])
    #gps_ms = gps_ms.reshape(len(gps_ms), 1)
    #gps_times = gps_s + (gps_ms * 10**(-3))
    #gps_ms = gps_ms.reshape(len(gps_ms), 1)
    #SNR = np.array(data.loc[:,"SNR"])
    #SNR = SNR.reshape(len(SNR), 1)
    #signif = np.array(data.loc[:,"signif"])
    #signif = signif.reshape(len(signif), 1)
    separate_by_sr = config.getboolean('DATA','separate_by_sr') #determine whether the user wants to only use data sampled at a certain sample rate
    if separate_by_sr: #if separate_by_sr = True in the config file:
        print("You have chosen to separate data based on sample rate.")
        sample_rate_files_path = config['DATA']['sample_rate_files_path'] 
        sr_target_channels = np.load(sample_rate_files_path) #load in a list of channels sampled at a certain rate
        sr_target_channels = np.array(sr_target_channels)    #turn it into an array
        for x,channel_name in enumerate(sr_target_channels):
            sr_target_channels[x] = str(channel_name.split(":")[1]) #remove the 'L1:' at the beginning of all the channel names

        print("---------------------------------PRINTING ALL CHANNELS ---------------------------------------------------------")
        print("Number of target channels:",len(sr_target_channels))
        print("Target channels:",sr_target_channels)

    features_to_keep = eval(str(config["DATA"]["features_to_keep"])) #add in list of desired features to train the ANN on
    target_indices = [] #empty list of target columns to remove from data
    for x,column in enumerate(data): #go thru each column and check the last part of the name to see feature selected, then if it isn't a desired feature, mark it for removal. Additionally, mark anything with a length of 1 after being split on underscores for removal. This removes the first 5 columns.

        if (column.split("_")[-1] not in features_to_keep) or (len(column.split("_")) < 2): 
            target_indices.append(x)
        elif separate_by_sr and ("_".join(column.split("_")[1:-3]) not in sr_target_channels): #if the user has chosen to separate based on sample rate, split the column name on underscores, look at the second through third to last parts of the split name, and if that name is in the target channel list, mark it for removal. (Strange slicing used to make the format of the names of the columns identical to the channel names generated by GWpy) 
            target_indices.append(x)

    target_indices = np.array(target_indices) #convert target_indices to an array so it can be fed to np.delete
    data = data.to_numpy()  #convert data to numpy array so destruction of non-desired data can commence
    print("data shape before:", data.shape)    #print shape of data before deleting columns
    data = np.delete(data, target_indices, 1) #delete all columns that aren't the desired type of data, according to the target_indices array.
    print("Data shape after deleting columns:", data.shape)   #print shape of data after deleting columns
    print("Data has been successfully separated into the desired datatype.")
    # removing any rows that are NaN (not a number)
    nan_num = 0           #declaring this variable and setting it to zero
    noNaNsense = []       #declaring this empty array
    for z in range(len(data)):
        if np.isnan(data[z]).any() == True:      #Paul:Does row z in our data matrix have any nans in 16 dimension?
            nan_num=nan_num+1                    #Paul:if so update nan_num by adding 1 to it
            noNaNsense=np.append(noNaNsense,[z],0)   #Paul:also append this row to noNaNsense array
    print("%d NAN's in this pat file"% nan_num)
    data = np.delete(data,noNaNsense,0)     #Paul:This command removes all rows in noNaNsense from the data array(the 0 is the search axis and tells the command to search for the rows of noNaNsense in the rows of data).
    # Count the unclean to clean ratio
    unclean = int(labels.sum())
    clean = len(data) - unclean
    print(f"Length of Data:{len(data)}, Glitch:{unclean}, Clean:{clean}") #Paul:print to terminal

    if balanced:             #Paul: This option makes equal sets of clean and unclean data
        # First sort the data and organize it so that the glitches come first and cleans come last
        print("Length of data/number of rows:",len(data))
        print("Length of label_indices:", len(label_indices))
        data = data[label_indices, :]
        # Next, pull only the first 2N samples where N is the number of glitches in the data set
        data = data[:2*unclean,:]       #slice twice as much data as there are glitches, pulling equal numbers of glitches and cleans.
        print(data)
        labels = labels[:len(data),:]    #slice as many labels as data
        print("Labels after being sliced:",labels)
        # Count number of glitch and clean again
        unclean = int(labels.sum())
        clean = len(data) - unclean
        print(f"Length of Data after Balancing:{len(data)}, Glitch:{unclean}, Clean:{clean}")    #Paul:print to terminal
    else:
        print("You are chosing to not balance the data")
    print("Shape of data:",data.shape)    
#Normalization function
# Transpose because we want to be normalizing over columns not rows and its easier to just switch the columns and rows then code for columns
  #  data=data.T
# Create an empty array with the shape of the data. This array will be filled with the data once the minmum number has been subtracted from each column
  #  shape=data.shape
  #  data_sub=np.empty(shape)
  #  for i in range(len(data)):
  #      data_sub[i]=data[i]-min(data[i])
# Normalize the data by dividing by the biggest number in each column. Axis=1 because we transposed it previously so we could subtract. 
  #  data_inputs_norm=normalize(data_sub,norm='max',axis=1)
   # inputs=data_inputs_norm.T







    print("Length of labels:",len(labels)) #print statement to provide sanity check
    print("Labels:",labels)
    print("Length of inputs:",len(inputs)) #print the input array and label array to see (manually) if they are the same (they should be the same) 
    print("Inputs:",inputs)
    return inputs, labels, clean, unclean


def numpy_read_data(DATA,LABELS, balanced=True):

    inputs=np.load(DATA)
    labels=np.load(LABELS)
    unclean=int(sum(labels))
    clean=len(labels) - unclean

    return inputs,labels,clean,unclean

def acc_over_epochs(history, filename, name):             #The function for plotting the Acc vs. Epochs plot
  # FIXME: Quinn should review this function
  hist = pd.DataFrame(history.history)       #Creates a Pandas DataFrame (a datatype) from collected history data
  hist['epoch'] = history.epoch              #Pulls the 'epoch' column in history DataFrame and sets it equal to hist['epochs']
  #num_epochs = len(hist['accuracy'])              #sets num_epochs equal to the number of rows in the history dataframe     
                                            
  plt.figure()    
  plt.xlabel('Epoch')
  plt.ylabel('Accuracy')
  plt.plot(hist['epoch'], hist['accuracy'])                                   #Plot Train Error 
  plt.plot(hist['epoch'], hist['val_accuracy'])                                   #Plot Val Error
  plt.legend(['Train accuracy', 'Val accuracy'])               #Creates Legend 
  plt.title(f'Accuracy vs. Epoch ({name})')
  plt.savefig(f'{filename}.png')          #Saves figure to path specified in function call
  plt.close()

def plot_val_loss(history,file_path):   #generate val_loss vs loss plot to investigate model overfitting to training data
    model_history = pd.DataFrame(history.history)
    model_history['epoch'] = history.epoch
    plt.plot(model_history['epoch'], model_history['loss']) #plot loss vs epoch
    plt.plot(model_history['epoch'], model_history['val_loss'])  #plot validation loss vs epoch
    plt.legend(["Loss","Validation Loss"])
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.title("Loss vs Validation loss over epochs")
    plt.show()
    plt.savefig(f'{file_path}.png')
    plt.close()


def roc_plot(fap, eff, eff_err, fap_err, filename, name, auc):         #Function for plotting the ROC curve
    x=np.linspace(0,1,100000)                           #Create set of one hundred thousand evenly spaced points between 0 and 1 for our random classifier  line (x=y)
    y=np.linspace(0,1,100000)

    fig,ax=plt.subplots()   #Begin our figure (note:everything above this has to take place in the same box)
    for e, se, f, sf in zip(eff, eff_err, fap, fap_err):
        ax.fill_between([f-sf, f+sf], [e-se]*2, [e+se]*2, color='blue', alpha=0.5)
    plt.title(f'Receiver Operating Characteristic (ROC) Curve (AUC = {auc})')    #Give the plot a title as specified in the function call
    ax.plot(fap, eff, drawstyle='steps', color='blue', marker='.')    
    ax.plot(x,y, color='red', dashes=[4,3])      #plot our random classifier line
    ax.semilogx()       #log scaling ib the x axis
    plt.xlabel('False Alarm Probability')
    plt.ylabel('Efficiency')
    plt.ylim([0,1])
    plt.xlim([1e-5,1e0])
    plt.savefig(f"{filename}_{name}_log.png")   #Save the figure to the path given in the function call
    plt.close()

    # Make linear plot
    fig, ax = plt.subplots()
    plt.title(f'Reciever Operator Characteristic (ROC) Curve (AUC = {auc})')
    for e, se, f, sf in zip(eff, eff_err, fap, fap_err):
        ax.fill_between([f-sf, f+sf], [e-se]*2, [e+se]*2, color='blue', alpha=0.5)
    ax.plot(fap, eff, drawstyle='steps', color='blue', marker='.')   
    ax.plot(x, y, color='red', dashes=[4,3])
    plt.xlabel('False Alarm Probability')
    plt.ylabel('Efficiency')
    plt.ylim([0,1])
    plt.xlim([0,1])
    plt.savefig(f"{filename}_{name}_linear.png")   #Save the figure to the path given in the function call
    plt.close()


def make_histogram(ann_snpred,ann_snlabels,filename,name):
    glitchsa = []
    cleansa = []
    for i, label in enumerate(ann_snlabels):
        if label == 0:
            cleansa.append(ann_snpred[i])
        elif label == 1:
            glitchsa.append(ann_snpred[i])
        else:
            print("Unexpected label!")
    glitchsa = np.array(glitchsa)
    cleansa = np.array(cleansa)
    plt.figure(figsize=(8,6))
    plt.hist(cleansa, bins=30, alpha=0.5, label="Cleans", color='springgreen')
    plt.hist(glitchsa, bins=30, alpha=0.5, label="Glitches",color='mediumslateblue')
    plt.xlabel("ANN ranking")
    plt.ylabel("Number of Clean or Glitch")
    plt.title('ANN')
    plt.legend(loc='upper right')
    plt.savefig(f"{filename}_{name}_histo.png")
    plt.show()
    plt.close()


#Function to split data into test and data


# A function to pull the test data from the input data
def pulling_test_from_dataset(data_inputs_b, data_labels_b, num_clean_b, num_unclean_b, num_clean_test, num_unclean_test):    
    # Pulling out num_unclean_test glitches and num_clean_test clean times, by taking advantage of their sorted state.
    # This function is used when the user wants to test against a set that is part of the training set. 
    # _b is for before because the function was overwriting itself. It stands for before, as in before this function was run.  


    # Logic checks to make sure there are enough clean and unclean instances to pull out the number of test points you want. If there are not enough glitches or cleans in the test set to pull out the desired number the system will quit, after giving a statment for why.  
    if num_unclean_b < int(num_unclean_test*2+50):
        print('You do not have enough glitches to pull from the training data to create duel sets')
        exit()
        
    if num_clean_b<int(num_clean_test*2+50):
        print('You do not have enough glitches to pull from the training data to create duel sets')
        exit()
        
    # First, sort the data and reverse it so glitches/uncleans come first
    sorted_indxs = np.argsort(data_labels_b, axis = None)
    data_inputs_sorted = data_inputs_b[sorted_indxs][::-1]
    data_labels_sorted = data_labels_b[sorted_indxs][::-1]

    # This pulls out the first set of data points which are ones (glitches/uncleans) 
    test_inputs_unclean = data_inputs_sorted[0:num_unclean_test]
    test_labels_unclean = data_labels_sorted[0:num_unclean_test]
        
    # This figures out where in the array we want to start the pull to get the clean times. Since the length of the array minus the number of points we want will give us a good start point to pull the correct numbers from the end of file.
    start_clean_indx=len(data_inputs_sorted) - num_clean_test

    # This pulls out the last set of data points which are zeros (cleans) since the data has been ordered and the last set of points in the array are going to be zeros if the data has passed the logic checks above. 
    test_inputs_clean = data_inputs_sorted[start_clean_indx:]
    test_labels_clean = data_labels_sorted[start_clean_indx:]

    #Putting it together for one big test arrays. All the ones are added to all the zeros arrays and renamed to title names that the ANN will recognize. 
    test_inputs = np.concatenate((test_inputs_unclean, test_inputs_clean))
    test_labels = np.concatenate((test_labels_unclean, test_labels_clean))

    #Removing the times in the test data from the overall data set, which leaves the training data as a unique data set.
    training_inputs = data_inputs_sorted[num_unclean_test:start_clean_indx]
    training_labels = data_labels_sorted[num_unclean_test:start_clean_indx]
    print(training_labels)
        

    return training_inputs, training_labels, test_inputs, test_labels
#--------------------------------------------------------------------------------------------------------

# Output files and paths
outpath = config['OUTPUT']['outpath']
run_name = config['OUTPUT']['run_name']
model_path = os.path.join(outpath,config['OUTPUT']['model_filename']) 
predictions_path = os.path.join(outpath,config['OUTPUT']['predictions_filename'])
labels_path = os.path.join(outpath,config['OUTPUT']['labels_filename'])
log_path = os.path.join(outpath,config['OUTPUT']['log_filename'])
acc_path = str(os.path.join(outpath,config['OUTPUT']['Acc']))
val_loss_path = str(os.path.join(outpath, config['OUTPUT']['val_loss_filename']))
save_model= config.getboolean('OUTPUT', 'save_model')
#model_save_path = config['OUTPUT']['model_save_path']
# Load in training and testing data
pat_data_format = config.getboolean('DATA', 'pat_data_format')
randomize = config.getboolean('DATA', 'randomize')
normalize_train = config.getboolean('DATA', 'normalize_train')
normalize_test = config.getboolean('DATA', 'normalize_test')
balance_train = config.getboolean('DATA', 'balance_train')
balance_test = config.getboolean('DATA', 'balance_test')
extract_test_data = config.getboolean('DATA', 'extract_test_data')
load_model= config.getboolean('DATA', 'load_model')
try:
    data = config['DATA']['input_data']
except:
    data = None
try:
    num_unclean_test = int(config['DATA']['num_unclean_test'])
except:
    num_unclean_test = None
try:
    num_clean_test = int(config['DATA']['num_clean_test'])
except: 
    num_clean_test = None
try:
    data_train = config['DATA']['data_train']
except:
    data_train = None
try:
    data_test = config['DATA']['data_test']
except:
    data_test = None
try:
    data_labels=config['DATA']['input_data_labels']
except: 
    data_labels=None
try:
    data_train_labels=config['DATA']['data_train_labels']
except: 
    data_train_labels=None
try:
    data_test_labels=config['DATA']['data_test_labels']
except:
    data_test_labels=None
try: 
    model_save_path = config['DATA']['model_save_path']
except:
    model_save_path=None


if not pat_data_format:
    if (data_labels is "") and (data_train_labels is "") and (data_test_labels is ""):
        raise ValueError("If you are passing numpy, you must provide data labels")
else:
    if (data_labels is not "") or (data_train_labels is not "") or (data_test_labels is not ""):
        print("Labels will be ignored")


if extract_test_data:
    if (data is "") or (num_unclean_test is None) or (num_clean_test is None):
        raise ValueError("If you have set the extract_test_data option, you must provide input_data, num_unclean_test, and num_clean_test")
    if (data_train is not "") or (data_test is not ""):
        print("data_train and data_test are being ignored.  Only input_data is being used.")
else:
    if (data_train is "") or (data_test is ""):
        raise ValueError("If you do not set the extract_test_data option, you must provide separate test and training data sets.")
    if (data is not "") or (num_unclean_test is not None) or (num_clean_test is not None):
        print("input_data, num_unclean_test, and num_clean_test will be ignored.")


if load_model:
    if (model_save_path is""):
        raise ValueError("Must have location to model")
    if (data_train is not "") or (data is not ""):
        print("You are loading a model to test on. Ignoring inputted train data")        
else:
    if (data is "") and (data_train is ""):
        raise ValueError("If you have are not loading a model, you must provide data to train on")
    if (model_save_path is not ""):
        print("You have chosen to train on passed data ignoring location to saved model")
# Load in hyperparameters (aka parameters set before the learning process begins). Lots of setting the variable to the correct data type. 
nodes = eval(str(config['HyperParameters']['Nodes']))
activation = eval(str(config['HyperParameters']['Activation']))
layers = int(config['HyperParameters']['Layers'])

# Check that the length of nodes and activation match the number of layers
assert len(activation)==(layers), "Must provide list of activation functions for each layer layers"
assert len(nodes)==(layers), "Must provide list of nodes for each layer"

output_nodes = int(config['HyperParameters']['Output_Nodes'])
output_activation = config['HyperParameters']['Output_Activation']
loss = config['HyperParameters']['loss']
metrics_compiler = config['HyperParameters']['metrics_compiler']
histogram_freq = int(config['HyperParameters']['histogram_freq'])
#batch_size_tensorboard = int(config['HyperParameters']['batch_size_tensorboard']) #batch size tensorboard doesn't exist anymore
write_graph = config.getboolean('HyperParameters', 'write_graph')
#write_grads = config.getboolean('HyperParameters', 'write_grads') #same for write_grads
write_images = config.getboolean('HyperParameters', 'write_images')
embeddings_freq= int(config['HyperParameters']['embeddings_freq'])
#embeddings_layer_names = eval(str(config['HyperParameters']['embeddings_layer_names'])) #same for embeddings_layer_names
embeddings_metadata = eval(str(config['HyperParameters']['embeddings_metadata']))
#embeddings_data = eval(str(config['HyperParameters']['embeddings_data'])) #same for embeddings_data
monitor = str(config['HyperParameters']['monitor'])
mode = config['HyperParameters']['mode']
min_delta = float(config['HyperParameters']['min_delta'])
patience = int(config['HyperParameters']['patience'])
monitor = str(config['HyperParameters']['monitor'])
monitor_modelcheckpoint = str(config['HyperParameters']['monitor_modelcheckpoint'])
verbose = config.getboolean('HyperParameters','verbose')
save_best_only = config.getboolean('HyperParameters','save_best_only')
save_weights_only = config.getboolean('HyperParameters','save_weights_only')
epochs = int(config['HyperParameters']['Epochs'])
batch_size = int(config['HyperParameters']['Batch_Size'])
opt = eval(str(config['HyperParameters']['Opt']))
dropout = eval(str(config['HyperParameters']['Dropout']))
L1_weight = eval(str(config['HyperParameters']['L1_weight']))
L2_weight = eval(str(config['HyperParameters']['L2_weight']))
# Load in things related to ROC curve making/plotting
sigma = int(config['ROC']['sigma'])
faps_path = os.path.join(outpath,config['ROC']['fap_filename'])
effs_path = os.path.join(outpath,config['ROC']['eff_filename'])
faps_err_path = os.path.join(outpath,config['ROC']['fap_err_filename'])
effs_err_path = os.path.join(outpath,config['ROC']['eff_err_filename'])
thresholds_path = os.path.join(outpath,config['ROC']['thresholds_filename'])
roc_path = os.path.join(outpath,config['ROC']['roc_filename'])
#Make output directory if it doesn't exist
#if not os.outpath.exists():     #os.path.dirname produces the directory of the given file path, os.path.exists returns whether or not that directory path exists
#    try:
#        os.makedirs(os.outpath)        #Uses os.makedirs to create the directory if was found to not exist
#    except OSError as exc: # Guard against race condition (Paul: I don't know what this part does)
#        if exc.errno != errno.EEXIST:
#            raised

#--------------------------------------------------------------------------------------------------------

###Get data

#If loading model only need test
if load_model:
    print("You are loading a model only test data will be imported")
    if pat_data_format:
        print("The data is being loaded as a  pat data file.")
        test_inputs, test_labels, num_clean_test, num_unclean_test = read_data(data, balanced=balance_train)
    else:
        print("The data is being loaded as a numpy file")
        test_inputs, test_labels, num_clean_test, num_unclean_train = numpy_read_data(data_test,data_test_labels) #getting testing data
        if balance_test:
            test_labels=test_labels.flatten()
            class_weights = class_weight.compute_class_weight('balanced', classes=np.array([0, 1]), y=test_labels)
            class_weights = {i:class_weights[i] for i in (0, 1)}
            print("Test class_weights:",class_weights)
        else:
            pass
    unclean_test = int(sum(test_labels))
    clean_test = len(test_labels) - unclean_test
    print(f"Test data set contains:{len(test_labels)}, Glitch:{unclean_test}, Clean:{clean_test}")    #Paul:print to terminal
    if normalize_test:
        print("You are normalizing the data")
# Transpose because we want to be normalizing over columns not rows and its easier to just switch the columns and rows then code for columns
        test_inputs=test_inputs.T
# Create an empty array with the shape of the data. This array will be filled with the data once the minmum number has been subtracted from each column
        shape=test_inputs.shape
        data_sub=np.empty(shape)
        for i in range(len(data)):
            data_sub[i]=test_inputs[i]-test_inputs(data[i])
# Normalize the data by dividing by the biggest number in each column. Axis=1 because we transposed it previously so we could subtract. 
        data_inputs_norm=normalize(data_sub,norm='max',axis=1)
        test_inputs=data_inputs_norm.T

    if randomize:
        print("You have chosen to shuffle/randomize your data sets.")
        test_inputs, test_labels = shuffle(test_inputs, test_labels)
    print("test",test_inputs.shape,test_labels.shape)
    assert len(test_inputs) == len(test_labels), "Evaluation data set input and label length not the same."
#If not loading model but creating one do this: 

else:
    if pat_data_format:
        print("The data is being loaded as a  pat data file.")
        if extract_test_data:
            print("The test data and training data are being pulled as two unique sets from the same input data.")
            data_inputs, data_labels, num_clean, num_unclean = read_data(data, balanced=balance_train)
            training_inputs, training_labels, test_inputs, test_labels = pulling_test_from_dataset(data_inputs, data_labels, num_clean, num_unclean, num_clean_test,num_unclean_test)
        else:
            print("The test data is a completely separate data set from the training data.")
            training_inputs, training_labels, num_clean_train, num_unclean_train = read_data(data_train, balanced=balance_train)
            test_inputs, test_labels, num_clean_test, num_unclean_test = read_data(data_test, balanced=balance_test)

    else: 
        print("The data is being loaded as a numpy file")
        if extract_test_data:
            print("The test data and training data are being pulled as two unique sets from the same input data.")
            data_inputs, data_labels, num_clean, num_unclean = numpy_read_data(data,data_labels)
            training_inputs, training_labels, test_inputs, test_labels = pulling_test_from_dataset(data_inputs, data_labels, num_clean, num_unclean,num_clean_test,num_unclean_test)
            if balance_train:
                training_labels=training_labels.flatten()
                class_weights = class_weight.compute_class_weight('balanced', classes=np.array([0, 1]), y=training_labels)
                class_weights = {i:class_weights[i] for i in (0, 1)}
                print("class_weights:",class_weights)
            else: 
                pass

        else:
            print("The test data is a completely separate data set from the training data.")
            training_inputs, training_labels, num_clean_train, num_unclean_train = numpy_read_data(data_train,data_train_labels) #getting training data
            test_inputs, test_labels, num_clean_test, num_unclean_train = numpy_read_data(data_test,data_test_labels) #getting testing data
            if balance_train:
                training_labels=training_labels.flatten()
                class_weights = class_weight.compute_class_weight('balanced', classes=np.array([0, 1]), y=training_labels)
                class_weights = {i:class_weights[i] for i in (0, 1)}
                print("Train class_weights:",class_weights)
            elif balance_test:
                test_labels=test_labels.flatten()
                class_weights = class_weight.compute_class_weight('balanced', classes=np.array([0, 1]), y=test_labels)
                class_weights = {i:class_weights[i] for i in (0, 1)}
                print("Test class_weights:",class_weights)

            else: 
                pass
    # Count number of glitch and clean for the users info
    unclean_train = int(sum(training_labels))
    clean_train = len(training_labels) - unclean_train
    print(f"Training data set contains:{len(training_labels)}, Glitch:{unclean_train}, Clean:{clean_train}")    #Paul:print to terminal

    unclean_test = int(sum(test_labels))
    clean_test = len(test_labels) - unclean_test
    print(f"Test data set contains:{len(test_labels)}, Glitch:{unclean_test}, Clean:{clean_test}")    #Paul:print to terminal
#Normalization function
    if normalize_train: 
# Transpose because we want to be normalizing over columns not rows and its easier to just switch the columns and rows then code for columns
        print("You are normalizing the training data")
        training_inputs=training_inputs.T
# Create an empty array with the shape of the data. This array will be filled with the data once the minmum number has been subtracted from each column
        shape=training_inputs.shape
        data_sub=np.empty(shape)
        for i in range(len(data)):
            data_sub[i]=training_inputs[i]-min(training_inputs[i])
# Normalize the data by dividing by the biggest number in each column. Axis=1 because we transposed it previously so we could subtract. 
        data_inputs_norm=normalize(data_sub,norm='max',axis=1)
        training_inputs=data_inputs_norm.T
    if normalize_test:
# Transpose because we want to be normalizing over columns not rows and its easier to just switch the columns and rows then code for columns
        print("You are normalizing the test data")
        test_inputs=test_inputs.T
# Create an empty array with the shape of the data. This array will be filled with the data once the minmum number has been subtracted from each column
        shape=test_inputs.shape
        data_sub=np.empty(shape)
        for i in range(len(data)):
            data_sub[i]=test_inputs[i]-min(test_inputs[i])
# Normalize the data by dividing by the biggest number in each column. Axis=1 because we transposed it previously so we could subtract. 
        data_inputs_norm=normalize(data_sub,norm='max',axis=1)
        test_inputs=data_inputs_norm.T

    if randomize:
        print("You have chosen to shuffle/randomize your data sets.")
        #test_inputs, test_labels = shuffle(test_inputs, test_labels)
        training_inputs, training_labels = shuffle(training_inputs, training_labels)
    print("test",test_inputs.shape,test_labels.shape)
    print("train",training_inputs.shape,training_labels.shape)
    assert len(training_inputs.T) == len(test_inputs.T) 
    assert len(training_inputs) == len(training_labels), "Training data set input and label length not the same."
    assert len(test_inputs) == len(test_labels), "Evaluation data set input and label length not the same."

    n_inputs = len(training_inputs.T)

#--------------------------------------------------------------------------------------------------------

if load_model:
    print("You are loading a previous model building or training will occur")
    model=keras.models.load_model(model_save_path)
    os.makedirs(outpath)

else: 
###Neural Network
    model=Sequential()                        #declare a sequential model using keras
    model.add(Dropout(0.5, input_shape=(n_inputs,)))
    model.add(Dense(units = nodes[0], activation= activation[0],input_dim = n_inputs,kernel_regularizer=l1_l2(l1=L1_weight[0],l2=L2_weight[0]))) #Initial hidden layer. NOTE use of just l2 regularizer here, could be changed back to l1_l2. 
    for x in range(layers-1):                       #Allows for the automatic addition of layers to the network. Each iteration of the for loop adds another matrix with the activation and nodes as specified by the matrices in our config file
        model.add(Dropout(dropout[x+1])) #Add dropout and dense layers so that dropout always comes after dense. 
        model.add(Dense(units = nodes[x+1], activation= activation[x+1],input_dim=n_inputs,kernel_regularizer=l1_l2(l1=L1_weight[x+1],l2=L2_weight[x+1])))
# FIXME: Explore whether activation function should be used on output layer.  Some sources indicate this could decrease accuracy.  We do want output to be between 0 and 1 though.
    model.add(Dropout(dropout[-1]))
    model.add(Dense(units= output_nodes, activation=output_activation))        #Add a final output layer

    model.summary()       #Output a model summary to the terminal

    if opt['name'] == 'Adam':                 #A series of elif statements that check a dictionary specified in the config file to determine which optimization algorithm to use(and with what arguments)
        optimizer=keras.optimizers.Adam(lr = opt['lr'])
    elif opt['name'] == 'Adamax':
        optimizer=keras.optimizers.Adamax(lr = opt['lr'])
    elif opt['name'] == 'Nadam':
        optimizer=keras.optimizers.Nadam(lr = opt['lr'])
    elif opt['name'] == 'SGD':
        optimizer=keras.optimizers.SGD(lr=opt['lr'])
    elif opt['name'] == 'RMSprop':
        optimizer=keras.optimizers.RMSprop(lr=opt['lr'])
    elif opt['name'] == 'Adagrad':
        optimizer=keras.optimizers.Adagrad(lr=opt['lr'])
    elif opt['name'] == 'Adadelta':
        optimizer=keras.optimizers.Adadelta(lr=opt['lr'])
    else: 
        print( '************Please enter Valid Optimizer Name: Adam, Adagrad, Adadelta, Adamax, Nadam, SGD or RMSprop************') 
			
    #model.compile(loss=loss, optimizer=optimizer, metrics=[metrics_compiler])     #Compile our model using the specified optimizer and the loss and metric functions specified in the config file

    #tensorboard=keras.callbacks.TensorBoard(log_dir=log_path, histogram_freq=histogram_freq, write_graph=write_graph, write_images=write_images, embeddings_freq=embeddings_freq, embeddings_metadata=embeddings_metadata)      # Tensorboard is a package used for visualization. It is used here for creating and storing log files, but as potentially wider applicability.
    #callbacks=[EarlyStopping(monitor=monitor,min_delta=min_delta,patience=patience,mode=mode),ModelCheckpoint(filepath=model_path,monitor=monitor_modelcheckpoint,verbose=verbose,save_best_only=save_best_only, save_weights_only=save_weights_only),tensorboard]     #Callbacks include Earlystopping which stops that fit early, ModelCheckpoint which saves the best model and tensorboard which saves log files.  
    
    model.compile(loss=loss, optimizer=optimizer, metrics=[metrics_compiler])     #Compile our model using the specified optimizer and the loss and metric functions specified in the config file
    tensorboard=keras.callbacks.TensorBoard(log_dir=log_path, histogram_freq=histogram_freq, write_graph=write_graph, write_images=write_images, embeddings_freq=embeddings_freq, embeddings_metadata=embeddings_metadata)      # Tensorboard is a package used for visualization. It is used here for creating and storing log files, but as potentially wider applicability.
    callbacks=[EarlyStopping(monitor=monitor,min_delta=min_delta,patience=patience,mode=mode),ModelCheckpoint(filepath=model_path,monitor=monitor_modelcheckpoint,verbose=verbose,save_best_only=save_best_only, save_weights_only=save_weights_only),tensorboard]     #Callbacks include Earlystopping which stops that fit early, ModelCheckpoint which saves the best model and tensorboard which saves log files.  
    print("Begin Training")
    #print(type(class_weights))
    #print(class_weights)
    #print(class_weights.keys())
    history = model.fit(training_inputs, training_labels, epochs=epochs, batch_size=batch_size, validation_split = 0.1, callbacks=callbacks)   #model.fit() trains network. Setting this equal to history allows us to make our Accuracy vs. Epochs plot later. Replace validation_split with validation_data = (test_inputs, test_labels) if you want it to validate on the test data.
    print("Done Training")
    # Plot the accuracy across epochs
    acc_over_epochs(history,acc_path,run_name)
    #Plot the val_loss and loss functions to investigate overfitting
    plot_val_loss(history,val_loss_path)
    print("Plotted val_loss vs loss")
    if save_model:
        model.save(outpath)
    else:
        pass

print("Making predictions")
np.save("not_test_inputs_in_keras_train", test_inputs)
np.save("not_ran_test_labels_in_keras_train",test_labels)
test_score=model.predict(test_inputs)           #Get the model prediction in probabilities for each test set event 
np.savetxt(f"{predictions_path}.txt",test_score)         #Save model predictions and labels
np.savetxt(f"{labels_path}.txt",test_labels)

print("Determine ROC curve metrics")
fap, eff, thresh=metrics.roc_curve(test_labels,test_score)         #run through different threshold values to produce the ROC curve

np.savetxt(f"{faps_path}.txt",fap)         #save fap, eff and thresh 
np.savetxt(f"{effs_path}.txt",eff)
np.savetxt(f"{thresholds_path}.txt",thresh)

auc=metrics.auc(fap,eff)    #Paul:area under curve

# Calculate error bars for ROC curve
eff_err=sigma*np.sqrt((1-eff)*eff/num_unclean_test)
fap_err=sigma*np.sqrt((1-fap)*fap/num_clean_test)
np.savetxt(f"{effs_err_path}.txt",eff_err)
np.savetxt(f"{faps_err_path}.txt",fap_err)
# Make Histogram
make_histogram(test_score,test_labels,roc_path,run_name)
# Make ROC curve
roc_plot(fap,eff,eff_err,fap_err,roc_path,run_name, auc)
print("Done!")

