import numpy as np

#---------------------------------
length=np.loadtxt("/home/kalista.wayt/public_html/hypertuned/ANN_ASC/chunkall_savemodel_balanced_1layer_train2_predictions_20patience/predictions.txt")
length=len(length)
predictions=np.zeros((length))
print("Shape of predictions:", predictions.shape)
sub_systems=['ASC','CAL','HPI','IMC','ISI','LSC','OMC','PEM','PSL','SQZ','SUS','TCS']
for i in sub_systems:
    load_path="/home/kalista.wayt/public_html/hypertuned/ANN_{}/chunkall_savemodel_balanced_1layer_train2_predictions_20patience/predictions.txt".format(i)
    sub_system_pred=np.loadtxt(load_path)
    print("Shape of sub_system_pred:", sub_system_pred.shape)
    predictions=np.column_stack((predictions,sub_system_pred))
    print("Shape of predictions:", predictions.shape)

print(predictions)
predictions=np.delete(predictions, 0,1)
print(predictions)
print("Shape of Predcitions should be (x,12):",predictions.shape)
np.save("/home/kalista.wayt/predictions_data_combined/hypertuned/chunkall_balanced_1layer_20patience.npy",predictions)


