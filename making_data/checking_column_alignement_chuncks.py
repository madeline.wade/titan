import numpy as np
import pandas as pd

#--------------------------------
#In shifting columns asserted that all days were saved in the same order of columns when generating them. Now check that chunks are the same 
input_path="/home/wayt1/copied_unseen_test_data/test_data/"
chunk01=pd.read_csv(input_path+"set01/day01/dataframe_chunk01_day01.csv")
chunk02=pd.read_csv(input_path+"set08/day01/dataframe_chunk08_day01.csv")

print("Checking each chunk matches:")
chunk1=list(chunk01.columns)
chunk2=list(chunk02.columns)
assert chunk1==chunk2
print("Chunks all lined up!")
