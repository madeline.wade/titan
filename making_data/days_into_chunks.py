import numpy as np

#-------------------------------------
chunks=['01','02','03']
days=['01','02','03']
iterator=['1','2','3']
for i in chunks:
    print("chunk{}".format(i))
    input_path="/home/wayt1/copied_unseen_test_data/test_data/set{}/".format(i)
    for (j,m) in zip(days,iterator):
        if m=='1':
            k=np.load(input_path+"day{}/whitened_train_features_col_alphabetize.npy".format(j))
            l=np.load(input_path+"/day{}/train_labels.npy".format(j))
            previous_day_data=k
            previous_day_labels=l
        elif m=='2':
            k=np.load(input_path+"day{}/whitened_train_features_col_alphabetize.npy".format(j))
            l=np.load(input_path+"/day{}/train_labels.npy".format(j))
            final_data=np.concatenate((previous_day_data,k),axis=0)
            final_labels=np.append(previous_day_labels,l)
        else:
            k=np.load(input_path+"day{}/whitened_train_features_col_alphabetize.npy".format(j))
            l=np.load(input_path+"/day{}/train_labels.npy".format(j))
            final_data=np.concatenate((final_data,k),axis=0)
            final_labels=np.append(final_labels,l)

    print("Shape of data:", final_data.shape)
    print("Length of labels:", len(final_labels))
    print("Number of glitches:", sum(final_labels))
    print("Number of cleans:",len(final_labels)-sum(final_labels))

    np.save("/home/wayt1/data_built/unseen_test_data/whitened_train_features_chunk{}.npy".format(i),final_data)
    np.save("/home/wayt1/data_built/unseen_test_data/labels_chunk{}.npy".format(i),final_labels)
