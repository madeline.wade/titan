import numpy as np

#---------------------------------
chunks=['01','02','03']
sub_systems=['ASC','CAL','HPI','IMC','ISI','LSC','OMC','PEM','PSL','SQZ','SUS','TCS']
for i in chunks:
    length=np.load("/home/kalista.wayt/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:ASC_balanced.npy".format(i))
    length=len(length)
    channels=np.zeros((length))
    print("Shape of channels:", channels.shape)
    for j in sub_systems:
        load_path="/home/kalista.wayt/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:{}_balanced.npy".format(i,j)
        sub_system_balance=np.load(load_path)
        print("Shape of sub_system_balance:", sub_system_balance.shape)
        channels=np.column_stack((channels,sub_system_balance))
        print("Shape of channels all:", channels.shape)

    print(channels)
    channels=np.delete(channels, 0,1)
    print(channels)
    print("Shape of channels should be (#,8440). It is:",channels.shape)
    np.save("/home/kalista.wayt/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:ALL_balanced.npy".format(i),channels)


