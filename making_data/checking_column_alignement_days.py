import numpy as np
import pandas as pd

#--------------------------------
#In shifting columns asserted that all days were saved in the same order of columns when generating them. Now check that chunks are the same 
chunks=['01','02','03']
for i in chunks:
    input_path="/home/wayt1/copied_unseen_test_data/test_data/set{}".format(i)
    day01=pd.read_csv(input_path+"/day01/dataframe_chunk{}_day01.csv".format(i))
    day02=pd.read_csv(input_path+"/day02/dataframe_chunk{}_day02.csv".format(i))
    day03=pd.read_csv(input_path+"/day03/dataframe_chunk{}_day03.csv".format(i))

    print("Checking each day matches:")
    day1=list(day01.columns)
    day2=list(day02.columns)
    day3=list(day03.columns)

    assert day1==day2
    assert day1==day3
    print("Days all lined up in chunk{}".format(i))
