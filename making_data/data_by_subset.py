import numpy as np
import pandas as pd
from os.path import exists

#----------------------------------------------------
#PICK HOW Many running; Is it train1, train2,and test? 
train2=False
test=False


chan=np.load("/home/wayt1/copied_unseen_test_data/test_data/set01/day01/train_feature_names.npy")

#BE CAREFUL: THIS should only be on if the data was built from multiple days thus forcing the need to order the columns alphabetically to get systems to align and thus you need the channel names on the dataframe to algin with what they really are!!!!!
chan=np.sort(chan)

#IF you are doing this over one day, or one chunk, where all the column names were aligned and you did not have to run shift_data_columens (so columns are not alphabetically aligned) comment out the previous line and start here

chunks=['08']

for i in chunks:
    print("chunk{}".format(i))
    input_path="/home/wayt1/copied_unseen_test_data/test_data/set{}/day01/not_whitened_train_features_col_alphabetize.npy".format(i)
    train1=np.load(input_path)
    df_train1=pd.DataFrame(train1,columns=chan)
    print(df_train1)

    x=0
    while x<len(chan):
        subset=[s[:6] for s in chan]
        savepath="/home/wayt1/subsystem_data/unseen_test_data/chunk{}/not_whitened_train_features_".format(i)+subset[x]
        print("Subset:",subset[x])
        print("Saveapth:",savepath)
        file_exists = exists(savepath+'.npy')
        #print("Path exists:", file_exists)
        if file_exists:
            print("This subset has already been created")
        else:
            print("Creating subset")
            yesorno=df_train1.columns.str.startswith(subset[x])
            array=train1[:,yesorno]
            np.save(savepath, array)
        x+=1

    if train2 == True: 
        train2=np.load("/home/wayt1/data_built/train2/whitened_train_features_20per_train_chunk{}.npy".format(i))
        df_train2=pd.DataFrame(train2,columns=chan)
        print(df_train2)

        x=0
        while x<len(chan):
            subset=[s[:6] for s in chan]
            savepath="/home/wayt1/subsystem_data/chunk{}/train2/whitened_train_features_".format(i)+subset[x]
            print("Subset:",subset[x])
            print("Saveapth:",savepath)
            file_exists = exists(savepath+'.npy')
            #print("Path exists:", file_exists)
            if file_exists:
                print("This subset has already been created")
            else:
                print("Creating subset")
                yesorno=df_train2.columns.str.startswith(subset[x])
                array=train2[:,yesorno]
                np.save(savepath, array)
            x+=1
    else: 
        pass
    if test==True: 
        test=np.load("/home/wayt1/data_built/test/whitened_train_features_20per_test_chunk{}.npy".format(i))
        df_test=pd.DataFrame(test,columns=chan)
        print(df_test)

        x=0
        while x<len(chan):
            subset=[s[:6] for s in chan]
            savepath="/home/wayt1/subsystem_data/chunk{}/test/whitened_test_features_".format(i)+subset[x]
            print("Subset:",subset[x])
            print("Saveapth:",savepath)
            file_exists = exists(savepath+'.npy')
            #print("Path exists:", file_exists)
            if file_exists:
                print("This subset has already been created")
            else:
                print("Creating subset")
                yesorno=df_test.columns.str.startswith(subset[x])
                array=test[:,yesorno]
                np.save(savepath, array)
            x+=1
    else: 
        pass
