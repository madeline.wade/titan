import numpy as np
import pandas as pd
#----------------------------------------------
chunks=['08']
days=['01']
for i in chunks: 
    for j in days:
        print("chunk{}, days{}".format(i,j))
        input_path="/home/wayt1/copied_unseen_test_data/test_data/set{}/day{}".format(i,j)
        save_path="/home/wayt1/copied_unseen_test_data/test_data/set{}/day{}".format(i,j)
        print(input_path)
        print(save_path)
        chan=np.load(input_path+"/train_feature_names.npy")
        data=np.load(input_path+"/not_whitened_train_features.npy")
        channels1=chan[8]
        print("Day{}:".format(j))
        df_day=pd.DataFrame(data,columns=chan)
        print(df_day)
        random_channel_before=df_day[channels1]
        df_day=df_day.reindex(sorted(df_day.columns),axis=1)
        print(df_day)
        random_channel_after=df_day[channels1]
        if not np.array_equal(random_channel_before,random_channel_after):
            print("Data has been shuffled from the wrong column")
        df_day.to_csv(save_path+"/dataframe_chunk{}_day{}.csv".format(i,j))
        day=df_day.to_numpy()
        print(day)
        np.save(save_path+"/not_whitened_train_features_col_alphabetize.npy",day)
    
