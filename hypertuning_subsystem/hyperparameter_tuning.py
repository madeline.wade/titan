#!/usr/bin/env python
# coding: utf-8
__usage__ = "python3 hyperparametertuning.py config.ini"
__description__ = "a hyperparemter tuning code for an ANN that is used for predicting the presence of glitches in strain data using auxiliary channels"
__author__ = "Kenyon LIGO lab"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#--------------------------------------------------------------------------------------------------------


import tensorflow as tf
from tensorflow import keras
import keras_tuner as kt
from kerastuner.tuners import BayesianOptimization
from sklearn.preprocessing import normalize
from sklearn.utils import shuffle
import pandas as pd
import numpy as np
import configparser
from optparse import OptionParser

#--------------------------------------------------------------------------------------------------------

#Config and option parser
parser = OptionParser(usage=__usage__, description=__description__)

opts, args = parser.parse_args()

config = configparser.ConfigParser()                           #set up the config parser by setting a variable equal to the ConfigParser class
config.read(args[0])                                           #read in config file from command line
#--------------------------------------------------------------------------------------------------------
# Output files and paths
project_name = config['OUTPUT']['project_name']
tensorboard_outpath= config['OUTPUT']['tensorboard']
# Load in training and testing data
data_train = config['DATA']['train_data']
train_labels=config['DATA']['train_labels']
data_test = config['DATA']['test_data']
test_labels=config['DATA']['test_labels']
#Load in hypertuning model params
max_trails=int(config['MODEL']['max_trails'])
executions_per_trail=int(config['MODEL']['executions_per_trail'])
num_layers=int(config['MODEL']['num_layers'])
kt_objective=config['MODEL']['kt_objective']
kt_direction=config['MODEL']['kt_direction']
early_stopping=config['MODEL']['early_stopping']
patience=int(config['MODEL']['patience'])
epochs=int(config['MODEL']['epochs'])
#Load in the options for hyperparamters
batch_size_min=int(config['HYPERPARAMS']['batch_size_min'])
batch_size_max=int(config['HYPERPARAMS']['batch_size_max'])
batch_size_steps=int(config['HYPERPARAMS']['batch_size_steps'])
nodes_min=int(config['HYPERPARAMS']['nodes_min'])
nodes_max=int(config['HYPERPARAMS']['nodes_max'])
nodes_step=int(config['HYPERPARAMS']['nodes_step'])
dropout_min=float(config['HYPERPARAMS']['dropout_min'])
dropout_max=float(config['HYPERPARAMS']['dropout_max'])
dropout_step=float(config['HYPERPARAMS']['dropout_step'])
activation_choices=eval(str(config['HYPERPARAMS']['activation_choices']))
optimizer_choices=eval(str(config['HYPERPARAMS']['optimizer_choices']))
lr_choices=eval(str(config['HYPERPARAMS']['lr_choices']))
regularizer_weight_choices=eval(str(config['HYPERPARAMS']['regularizer_weight_choices']))
regularizer_choices=eval(str(config['HYPERPARAMS']['regularizer_choices']))
#----------------------------------------------------------------------------------------------------------
def kali_read_data(DATA,LABELS, balanced=True):

    inputs=np.load(DATA)
    labels=np.load(LABELS)
    unclean=int(sum(labels))
    clean=len(labels) - unclean

    return inputs,labels,clean,unclean

def auc(y_true, y_pred):
    auc = tf.metrics.auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return(auc)



train_inputs, train_labels, train_clean, train_unclean = kali_read_data(data_train,train_labels) #getting training data
test_inputs, test_labels, test_clean, test_unclean = kali_read_data(data_test,test_labels) #getting testing data


train_inputs, train_labels = shuffle(train_inputs, train_labels)
test_inputs, test_labels = shuffle(test_inputs, test_labels)




def build_hypermodel(hp):
    #this function builds the model for hp tuning
    model = keras.Sequential() #model type is sequential
    
    hp_regularizer = hp.Choice('regularizer', values = regularizer_choices) #give choices for regularizer, set the choice to hp_regularizer
    hp_weight = hp.Choice('weight', values = regularizer_weight_choices) #give choices for regularizer weight, set the choice to hp_weight
    if hp_regularizer == 'L1': #if choice is L1, use hp_weight
            regularizer = keras.regularizers.l1(hp_weight)
    elif hp_regularizer == 'L2': #if choice is L2, use hp_weight
            regularizer = keras.regularizers.l2(hp_weight)
    elif hp_regularizer == 'l1_l2':#if choice is l1_l2, choose a different value for weight, set it to hp_weight_l1l2
        with hp.conditional_scope('regularizer', ['l1_l2']): 
            hp_weight_l1l2loop = hp.Choice('weightl1l2', values = regularizer_weight_choices)
            regularizer = keras.regularizers.l1_l2(l1 = hp_weight, l2 = hp_weight_l1l2loop) 
    
    model.add(keras.layers.Dense(units = hp.Int('nodes', min_value = nodes_min, max_value = nodes_max, step = nodes_step), activation = hp.Choice('activation', 
        values = activation_choices), 
        kernel_regularizer = regularizer,
        input_dim = (len(train_inputs.T)))) #add a layer with a choice for number of nodes, choice for activation, the regularizer determined above, and input dimensions based on the training inputs from the dataset
    model.add(keras.layers.Dropout(rate = hp.Float('dropout', min_value = dropout_min, max_value = dropout_max, step = dropout_step))) #add dropout layer with a choice for dropout rate
    
    for i in range(num_layers - 1): 
        #sets up a loop to add more layers in similar fashion to the first one, with choices for number of nodes, activation, dropout, weight, and regularizer for each layer
        hp_units = hp.Int('nodes_%d' %i, min_value = nodes_min, max_value = nodes_max, step = nodes_step)
        hp_activation = hp.Choice('activation_%d' %i, values = activation_choices)
        hp_dropout = hp.Float('dropout_%d' %i, min_value = dropout_min, max_value = dropout_max, step = dropout_step)
        hp_weightloop = hp.Choice('weight_%d' %i, values = regularizer_weight_choices)
        hp_regularizerloop = hp.Choice('regularizer_%d' %i, values = regularizer_choices)
        
        if hp_regularizerloop == 'L1': #same process as before with the regularizers but now for within the loop
            regularizer = keras.regularizers.l1(hp_weightloop)
        elif hp_regularizerloop == 'L2':
            regularizer = keras.regularizers.l2(hp_weightloop)
        elif hp_regularizerloop == 'l1_l2':
            with hp.conditional_scope('regularizer_%d' %i, ['l1_l2']):
                hp_weight_l1l2loop = hp.Choice('weightl1l2_%d' %i, values = regularizer_weight_choices)
                regularizer = keras.regularizers.l1_l2(l1 = hp_weightloop, l2 = hp_weight_l1l2loop)
            
        model.add(keras.layers.Dense(units = hp_units, activation = hp_activation,
                kernel_regularizer = regularizer)) #adds the dense layer that has the choice values from within the loop        
        model.add(keras.layers.Dropout(hp_dropout)) #adds the dropout layer that has the choice values from within the loop

    model.add(keras.layers.Dense(units = 1, activation = 'sigmoid')) #adds the last layer
    
    hp_learning_rate = hp.Choice('learning_rate', values = lr_choices) #gives choices for learning rate, sets choice to hp_learning_rate
    hp_optimizer = hp.Choice('optimizer', values = optimizer_choices) #gives choices for optimizer, sets choice to hp_optimizer
    
    if hp_optimizer == 'SGD':
        optimizer = keras.optimizers.SGD(learning_rate = hp_learning_rate)
    elif hp_optimizer == 'RMSprop':
        optimizer = keras.optimizers.RMSprop(learning_rate = hp_learning_rate)
    elif hp_optimizer == 'Adam':
        optimizer = keras.optimizers.Adam(learning_rate = hp_learning_rate)
    elif hp_optimizer == 'Adadelta':
        optimizer = keras.optimizers.Adadelta(learning_rate = hp_learning_rate)
    elif hp_optimizer == 'Adagrad':
        optimizer = keras.optimizers.Adagrad(learning_rate = hp_learning_rate)
    elif hp_optimizer == 'Adamax':
        optimizer = keras.optimizers.Adamax(learning_rate = hp_learning_rate)
    elif hp_optimizer == 'Nadam':
        optimizer = keras.optimizers.Nadam(learning_rate = hp_learning_rate)
    #sets up optimizer depending on the hp_optimizer, uses hp_learning_rate
    model.compile(optimizer = optimizer, loss = keras.losses.binary_crossentropy, metrics = ['AUC']) #compiles model using optimizer, sets up loss and metric

    return model #returns the final model




class MyTuner(kt.tuners.BayesianOptimization):
    #sets up the tuner to use Bayesian Optimization
    def run_trial(self, trial, *args, **kwargs):
        kwargs['batch_size'] = trial.hyperparameters.Int('batch_size', min_value = batch_size_min, max_value = batch_size_max, step = batch_size_steps) #gives choices for bath size, uses batch_size as a keyword argument in run_trial
        return super(MyTuner, self).run_trial(trial, *args, **kwargs) #runs the trial



objective= kt.Objective(kt_objective, direction = kt_direction)
tuner = MyTuner(build_hypermodel, objective,
                        max_trials = max_trails, overwrite = True, executions_per_trial = executions_per_trail, project_name = project_name) #calls MyTuner using the model that was set up, tells it to maximize auc, gives it a number of trials and executions per trial



tuner.search(train_inputs, train_labels, epochs = epochs, validation_data = (test_inputs, test_labels), 
             callbacks=[keras.callbacks.TensorBoard(tensorboard_outpath), keras.callbacks.EarlyStopping(early_stopping, patience = patience)]) #sets number of epochs, validation data, and callbacks. callbacks include the set up for tensorboard and EarlyStopping
best_hps = tuner.get_best_hyperparameters(1)[0]

print(tuner.results_summary()) #gives results from 10 best trials at the end of the run
print(tuner.search_space_summary()) #gives search space summary at th end of the run




